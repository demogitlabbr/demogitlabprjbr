FROM ubuntu:latest
COPY hello.py /
RUN apt-get -y update && apt-get -y install python3-dev python3-pip
RUN pip3 install Flask
ENTRYPOINT [ "python3" ]
CMD ["/hello.py"]